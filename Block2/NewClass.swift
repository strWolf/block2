//
//  NewClass.swift
//  Block2
//
//  Created by student08 on 14.10.17.
//  Copyright © 2017 student04. All rights reserved.
//

import UIKit

class NewClass: UITableViewController {
    
    /*
     *1. написати метод що виводить привітання “hello word”
     */
    static func helloWorld(){
        print("hello word")
    }
    
    /*
     *2. написать метод который будет выводить приветствие передаваемое аргументом количество раз
     */
     static func helloWorldOutCount(outCount: Int){
        for _ in 0..<outCount{
            helloWorld()
        }
    }
    
    /*
     *3. написать метод в который передается номер дня(1-7),  и выводится строка с названием дня недели
     */
    static func daysOfWeek(outCount: Int){
        switch outCount{
        case 1:
            print("Monday")
        case 2:
            print("Tuesday")
        case 3:
            print("Wednesday")
        case 4:
            print("Thursday")
        case 5:
            print("Friday")
        case 6:
            print("Saturday")
        case 7:
            print("Sunday")
        default:
            print("Week")
        }
    }
    
    /*
     *4. сделать метод 3 не зависимым от номера дня (8- понедельник, 9 - вторник и т д)
     */
    static func daysOfWeekAllNumber(outCount: Int){
        if outCount <= 7{
            daysOfWeek(outCount: outCount)
        }
        else{
            daysOfWeek(outCount: outCount%7)
        }
    }
    
    /*
     *5. Числа Фибоначчи — линейная последовательность натуральных чисел, где первое и второе равно единице, а каждое последующее — сумме двух предыдущих
     */
    static func numberFibanachi(outCount: Int){
        var first = 0
        var second = 1
        var three = 0
        if outCount != 0{
            for _ in 1..<outCount{
                three = first + second
                first = second
                second = three
            }
        }
        else{
            second = 0
        }
        print("Число Фибоначчи \(outCount) =", second )
    }
    
    /*
     *6. Дюймы в сантиметры 1 дюйм = 2.54см
     */
    static func inchToCentimeters(outCount: Int){
        var returnCentimetrs = 0.0
        returnCentimetrs = Double(outCount) * 2.54
        print("\(outCount) дюймов = \(returnCentimetrs) см")
    }
    
    /*
     *7. Сколько можно петь эту песню:
     99 bottles of beer on the wall.
     99 bottles of beer.
     You take one down, pass it around. 98 bottles of beer on the wall.
     98 bottles of beer on the wall. . . .
     */
    static func singSong(outCount: Int){
        for i in 0..<outCount{
            print("\(outCount - i) bottles of beer on the wall.")
            print("\(outCount - i) bottles of beer. \nYou take one down, pass it around.")
        }
    }
    
    /*
     *8. Написать программу которая выводит все числа которые можно нацело делить введенным числом в рамках 1…100
     */
    static func divideWhole(outCount: Int){
        print("Числа которые можно нацело делить введенным числом в рамках 1…100")
        for i in 1..<101{
            if i%outCount == 0{
                print(i)
            }
        }
    }
}
